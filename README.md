# **[aLook Analytics App](http://www.alookanalytics.com/#!alook-app/ci4t)**
A custom data-science application in [Keboola Connection](https://www.keboola.com/)

**Contact:** Adam Votava (adam.votava@alookanalytics.com); [aLook Analytics](http://www.alookanalytics.com/)

> **An easy way towards tailor-made analytical models**

> - What is the app for
> - 7 steps to tailor-made analytical model
> - Input configuration in KBC

## What is the app for

Thanks to Keboola and the new aLook Analytics app, the main integration and collaboration issues while implementing analytical models are gone. 
 
The beauty of the solution at hand lies in a simple integration of custom analytics into the standard data processes. All the issues around data processing, data transfer and model deployment have been already sorted out, and therefore the primary focus really is on solving the business problem. No middle-man needed. Your data guy will deal with data processing, our data scientist will train the best possible model in R and finally your business person will make sure that you will be able to make more money using this new information.
 
**Predictive modelling embedded in data processes**
 
The job of aLook Analytics is to provide the client with tailor-made predictive models developed in R that directly support their KPIs. Some specific examples of these models are:
 
- **Propensity models** – predicting, which customers are likely to buy a product, start using a product, decrease their activity or even churn completely
- **Recommendation engines** – providing a personalized product offer for each customer
- **Direct campaign optimization** – models indicating who to target, when, with which offer and through which channel

We have expertise in building such models in various industries – retail banking and e-commerce, but also behavioral talent analytics and sport.

If you want to give it a try, start by contacting us at **adam@alookanalytics.com** or **kbcapp@alookanalytics.com**. The process is fairly straightforward. After the initial check-in of what the business problem is, we put together a solution plan, exchange a data sample to develop an initial model and after a few very quick iteration loops (days) we can deploy the model into your standard data processes to start acting upon it. It’s easy and fun. You will see :).

## 7 steps to tailor-made analytical model

We are used to have impact on your business (profit/revenue/sales) set as our KPI and we use data-driven analytical approach to succeed. We believe that an acceptable first wave solution has to be found within 2-3 months tops and then constantly improved if necessary. That is because the business priorities are changing all the time and analytics need to reflect that. It is about step-by-step improvements in solving real-life business problems, not designing an overly complex solution that would take years to build.

The process consists of 7 steps (with steps 1-6 being iteratively repeated till an acceptable solution is found; or fail fast and redefine the business problem):

1. **Business understanding of the problem** (e.g. increase response rate, decrease churn…)
2. **Defining the way to approach the problem** (recommendation engine, propensity-to-buy algorithm, segmentation, customer lifetime value etc.)
3. **Understanding what data is needed and what is available**
4. **Data preparation and feature engineering**
5. **Modelling / machine learning algorithm**
6. **Piloting, testing and evaluation** (validate and test the solution with the end-user)
7. **Deployment / roll-out** (make it happen!)

We keep the process as convenient as possible yet informative enough for you to feel fully informed the whole time.

The client needs to be involved in the following steps

- initial meeting/call/video conference to lay down the problem
- description of the relevant available data
- handover of the data sample to be used for modelling (via KBC)
- model evaluation session
- model deployment to KBC
- next improvement loop / full roll-out

All models are kept in our private repository on Bitbucket and their management is very easy. We can develop a new version of a model and switch it to production in no time.

## Input configuration in KBC

Deployment of the application is very easy.

The setting step for the end-user of aLook Analytics app in Keboola Connection includes just mapping of input and output tables. Any other extra configurations need to be added only in case when multiple complex models are used in parallel.

`Input mapping` defines the data used as input for the model. Please keep in mind that the R model

- expects the agreed name of the csv file
- expects columns in the file that were agreed after the model evaluation
- is case sensitive

`Output mapping` defines where the scored data will be stored. Again, the name of the output csv file needs to be the same as agreed before the model deployment.

`Configuration data` can stay empty unless specifically agreed with us. An example when we need further specification is when there are multiple models deployed at one time and we need to specify which model should be used in which process.

The following picture shows an example of a full configuration.

![aLook app preview](https://bytebucket.org/alookanalytics/kbc.custom.app.public/raw/c7f34c215c6e2fe91b9b26608832d387b7dc789e/screen_shots/aLook_app_screen.png)

In case you have any questions please let us know. We will gladly help you.